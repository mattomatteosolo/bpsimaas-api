# BPSimaaS web api

WEB API for BPSIMaaS.

This REST API service is made with
[Flask](http://flask.palletsprojects.com/en/1.1.x/), python microframework for
web, highly extensible, equipped with a lot of plug-in, extensions and can count
on a vast community. Particularly, this package will use
[Flask-RESTful](https://flask-restful.readthedocs.io/en/latest/), very helpfull
in creating API REST, like the name suggests.

## How to use the package

### Reproduce with python 3.10

Clone repo

ssh
```bash
$ git clone git@bitbucket.org:mattomatteosolo/bpsimaas-api.git
```

https
```bash
$ git clone https://<youraccountgoeshere>@bitbucket.org/mattomatteosolo/bpmn4bpsim.git
```

Create virtual environment and install dependencies, installing dependencies
actually require you to enable ssh for bitbucket since this package relies on
[bpmn4bpsim](https://bitbucket.org/mattomatteosolo/bpmn4bpsim/src/master/) and
uses pip with ssh. If you don't want to

```bash
$ python -m venv venv
$ . venv/bin/activate # . is bash alias for source
$ # deactivate or restart bash shell to get rid of venv related stuff
$ pip install -r requirements.txt
```

Before running app, don't forget to prepare [mongodb](#mongo), both the deamon
and the configuration file. We also require you to provide a config file named
`.config.json` (see example\_config.json).

Run app

```bash
$ python app.py
```

### Database {#mongo}

First thing first, before actually running the app: set up mongo cluster.

#### MongoDB auth set up

To install mongo db i suggest to use a system package manager, otherwise follow
the instruction found on the [mongo guide](https://docs.mongodb.com/manual/administration/install-community/).

Following guide to configure mongo is fine for system that use
[systemctl](https://www.redhat.com/sysadmin/getting-started-systemctl).

1.  start mongo deamon is something like

    ```bash
    $ sudo systemctl start mongodb.service
    ```

1.  enter mongo

    ```
    $ mongo
    ```

1.  (if it doesn't exist yet) create root account

    ```
    > use admin
    > db.createUser(
        {
            user: "<root-name>",
            pwd: passwordPrompt(),
            roles: ["root"]
        }
    )
    // click enter and type desired password
    ```

1.  restart mongo deamon (as before)

    ```bash
    $ sudo systemctl restart mognodb.service
    ```

1.  modify configuration file for mongo (default for linux in
    /etc/mongodb.conf), updating

    `security` section

    ```yaml
    security:
        authorization: "enabled"
    ```

    and you'll end up with something looking like this

    ```
    # mongod.conf

    # Where and how to store data.
    storage:
      dbPath: /var/lib/mongodb
      journal:
        enabled: true
    #  engine:
    #  mmapv1:
    #  wiredTiger:

    # where to write logging data.
    systemLog:
      destination: file
      logAppend: true
      path: /var/log/mongodb/mongod.log

    # network interfaces
    net:
      port: 27017
      bindIp: 127.0.0.1


    # how the process runs
    processManagement:
      timeZoneInfo: /usr/share/zoneinfo

    security:
      authorization: "enabled"

    ```

    For complite documentation of all options, see 
    [this](http://docs.mongodb.org/manual/reference/configuration-options/)


1.  enter with new root user and password

    ```
    $ mongo -u <root-name> -p --authenticationDatabase admin
    ...
    Password: <root-password>
    ```

1.  add BPSimaaS db admin

    ```
    > use BPSimaaS
    > db.createUser(
        {
            user: "<admin-name>",
            pwd: passwordPrompt(),
            roles: [{role:"dbAdmin", db:"BPSimaaS"}]
        }
    )
    ```

1.  as said before, create a `.config.json` file and fill it with data it needs

## Generate documentation

After the creation of the virtual environment, python ships with a tool called
pydoc that can be used to create documentation after
[docstrings](https://www.python.org/dev/peps/pep-0257/)

```bash
pydoc -b <module-path>
```

## Authentication and Autorization

To authenticate, simply use username/email and password. 

To authorize operation, once authenticated, the app uses JSON Web Token (JWT),
with flask extension
[Flask-JWT-Exetended](https://flask-jwt-extended.readthedocs.io/en/stable/).


## Run the app

Once installed necessary dependencies run

```bash
$ python app.py
```

## Test the API

To test API it is suggested to use tool like 
[Postman](https://www.getpostman.com/), that allow you to perform any kindo of
http requests.

// TODO, "automatic" API tests

## API Explanation

For a better explanation of the api end point look at [this](API_ENDPOINT.md).


