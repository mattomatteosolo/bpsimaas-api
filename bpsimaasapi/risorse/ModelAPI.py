"""Module of Model resource

API
-----------
{
    _id: ObjectId,
    nomdeModello: string,
    emailUtente: string,
    scenari: List[Scenario]
}
"""
from bson import ObjectId
from flask_restful import Resource
from flask import request, make_response
from flask_jwt_extended import jwt_required, get_jwt_identity
from flask_pymongo import GridFS
from lxml import etree
from os import path
from bpsimaasapi.risorse import schema_directory
from bpsimaasapi.mongosetup import mongo


class ModelAPI(Resource):
    """
    Model reource before being loaded

    Methods
    -------
    get()
        List of model with id and name
    post()
        Took a model in xml format and load it in the system
    """

    @jwt_required()
    def get(self):
        """
        Vedi modelli caricati

        Returns
        -------
        application/json
            Restituisce sempre un oggetto JSON, con unico campo modelli: list,
            eventualemente lista vuota
        """
        jwti = get_jwt_identity()
        if mongo.db is None:
            return {"msg": "Error connecting to database"}, 500
        model_query = mongo.db.modelli.find(
            {"emailUtente": jwti}, {"nomeModello": 1}
        )
        model_list = []
        for model in model_query:
            model_list.append(
                {
                    "_id": str(model.get("_id")),
                    "nomeModello": model.get("nomeModello"),
                }
            )
        return {"modelli": model_list}

    @jwt_required()
    def post(self):
        """
        File inclusion of bpmn model. If a model with the same name
        already exists for the current user, return an error

        Parameters
        ----------
        modello : etree._ElelemntTree
            bpmn model sent in the body, Content-Type:
            multipart/form-data
        """
        # Recupera il file dalla richiesta
        identity = get_jwt_identity()
        if not request.files:
            return {"msg": "No file was given"}, 400

        model = request.files.get("modello")
        if not model:
            return {"msg": "No model was given"}, 400
        modelName = model.filename
        if not modelName:
            return {"msg": "Nome del modello non specificato."}, 400

        # if the user alreay possess a model with the given name
        if mongo.db is None:
            raise ConnectionError()
        model_q = mongo.db.modelli.find_one(
            {"emailUtente": identity, "nomeModello": modelName}
        )
        if model_q:
            return {
                "msg": f"Model named {modelName} already exists. Try to"
                "delete it before"
            }, 400

        # Model validation via BPMN schema, remember, Eclipse does some
        # wrong
        if not path.splitext(modelName)[-1] == ".bpmn":
            return {
                "msg": "Error during file acquisition",
                "dettagli": "Required file with bpmn extension",
            }, 400
        try:
            tree = etree.parse(model)
            with open(
                path.join(path.abspath(schema_directory), "BPMN.xsd")
            ) as f:
                schemaDoc = etree.parse(f)
            schema = etree.XMLSchema(schemaDoc)
            schema.assertValid(tree)
        except etree.DocumentInvalid as e:
            # TODO: Security concern, maybe to much info
            return {
                "msg": "Model validation error",
                "errorName": e.__class__.__name__,
                "errorMsg": "" if not e.args else e.args,
            }, 400
        except etree.XMLSyntaxError as e:
            # TODO: Security concern, maybe to much info
            return {
                "msg": "Model validation error",
                "errorName": e.__class__.__name__,
                "errorMsg": "" if not e.args else e.args,
            }, 400
        except Exception as e:
            # TODO: Security concern, maybe to much info
            return {
                "msg": "Error during model loading",
                "errorName": e.__class__.__name__,
                "errorMsg": "" if not e.args else e.args[0],
            }, 400

        # Inserisci il nuovo file, facendo attenzione di eliminare le prove
        _id = ObjectId()
        GridFS(mongo.db).put(
            etree.tostring(tree, pretty_print=True),
            filename=modelName,
            base="fs",
            content_type="application/bpmn+xml",
            _id=_id,
        )
        # aggiorna la collezione dei modelli
        insert_q = mongo.db.modelli.insert_one(
            {
                "_id": _id,
                "nomeModello": modelName,
                "emailUtente": identity,
                "scenari": [],
            }
        )
        if not insert_q.inserted_id:
            return {"msg": "Error while inserting model into database"}, 500
        return {
            "msg": "Model loaded successfully",
            "inserted_id": str(insert_q.inserted_id),
            "nomeModello": modelName,
        }, 201


class LoadedModelAPI(Resource):
    """
    Resource bodel after being loaded

    Methods
    -------
    get(idModello:str)
        Retrun the same bpmn model in xml format that was previously loaded
    put(idModello:str,data:request.data)
        Update model data (for now only name)
    delete(idModello)
        Delete model
    """

    @jwt_required()
    def get(self, idModello: str):
        """
        Retrun the same bpmn model in xml format that was previously loaded,
        with no bpsim parameterization

        Parameters
        ----------
        idModello : str
            id of the model to return

        Returns
        -------
        JSON
            if some error occurs
        bpmn+xml
            bpmn model in xml format
        """
        objectId = ObjectId(idModello)
        if mongo.db is None:
            return {"msg": "Error while connecting to database"}
        if not mongo.db.modelli.find_one(
            {"_id": objectId, "emailUtente": get_jwt_identity()}
        ):
            return {
                "msg": f"Error: user with email {get_jwt_identity()} doesn't"
                f"own any model with id{idModello}"
            }, 404

        # retrieve data from fs
        gridfs = GridFS(mongo.db)
        if gridfs.exists(ObjectId(idModello)):
            file = gridfs.find_one({"_id": ObjectId(idModello)})
            model = etree.parse(file).getroot()
            relationshipData = model.xpath(
                '//*[local-name()="relationship" and @type="BPSimData"]'
            )
            for item in relationshipData:
                model.remove(item)
            response = make_response(etree.tostring(model))
            response.mimetype = "application/bpmn+xml"
            return response

        return {
            "msg": "Error in our database",
            "dettagli": f"No model found in memory with id {idModello}",
        }, 500

    @jwt_required()
    def put(self, idModello):
        """
        PUT update model data

        Parameters
        ----------
        idModello : str
            id of the model to update

        Returns
        -------
        JSON
            mostly metadata about the operation outcome
        """
        if (
            request.content_length and not request.content_length > 0
        ) or not request.files:
            return {"msg": "No data was given to update the model"}, 400

        model = request.files.get("modello")
        if not model:
            return {"msg": "No file with key modello"}, 400

        if mongo.db is None:
            return {"msg": "impossible to connect to database"}, 500

        # delete old model
        gridfs = GridFS(mongo.db)
        gridfs.delete(ObjectId(idModello))
        # validate and update
        try:
            tree = etree.parse(model)
            with open(
                path.join(path.abspath(schema_directory), "BPMN.xsd")
            ) as f:
                schemaDoc = etree.parse(f)
            schema = etree.XMLSchema(schemaDoc)
            schema.assertValid(tree)

        except etree.XMLSchemaValidateError:
            return {"msg": "Validation Error: not a valid BPMN model"}, 400

        except Exception:
            return {"msg": "Validation Error: not a valid BPMN model"}, 400

        _id = gridfs.put(
            model,
            _id=ObjectId(idModello),
            content_type="application/bpmn+xml",
            base="fs",
            filename=model.filename,
        )
        if str(_id) != idModello:
            gridfs.delete(_id)
            return {"msg": "Uknown error during updating"}, 500

        # aggiorna nome modello
        file_name = model.filename
        name_update_q = mongo.db.modelli.update_one(
            filter={
                "_id": ObjectId(idModello),
            },
            update={"$set": {"nomeModello": file_name}},
        )
        if name_update_q.matched_count != 1:
            return {"msg": "Uknown error during updating"}, 500
        if name_update_q.modified_count != 1:
            return {"msg": "Uknown error during updating"}, 500

        return {"msg": "Model updated successfully"}

    @jwt_required()
    def delete(self, idModello: str):
        """
        DELETE delete selected model

        Parameters
        ----------
        idModello : str
            model's id that needs to be deleted

        Returns
        -------
        JSON
            metadata
        """
        if mongo.db is None:
            raise ConnectionError()

        gridfs = GridFS(mongo.db)
        fileOut = gridfs.find_one(ObjectId(idModello))
        if not fileOut:
            return {"msg": f"No model found with id {idModello}"}, 404
        fileName = fileOut.filename
        # prima di cancellare il modello, controllarne la proprietà
        check_property_query = mongo.db.modelli.find_one(
            filter={"emailUtente": get_jwt_identity(), "nomeModello": fileName}
        )

        # se possiede l
        if check_property_query:
            gridfs.delete(ObjectId(idModello))
            queryDelete = mongo.db.modelli.delete_one(
                filter={
                    "emailUtente": get_jwt_identity(),
                    "_id": ObjectId(idModello),
                }
            )

            if queryDelete.deleted_count == 1:
                return {}, 204

            return {"msg": "Uknown error during model deletion"}, 500

        return (
            {
                "msg": f"{get_jwt_identity()} doesn't own a model with id"
                f"id {idModello}"
            },
            404,
        )
