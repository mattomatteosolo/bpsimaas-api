"""
Module for ElementParameters resource

API Risorsa
-----------
{
    "elementRef": `str`, // required
    "parametersId": `str`,
    "parametersGroup": `ParametersGroupOneOf`, // disambiguation
    "vendorExtensions": `List[VendorExtentions]`
}
"""
from bson import ObjectId
from flask_jwt_extended import get_jwt_identity, jwt_required
from flask_restful import Resource, request
from flask_pymongo import GridFS
from lxml import etree
from marshmallow import ValidationError

from bpsimaasapi.mongosetup import mongo
from bpmn4bpsim.bpsimclasses import ParamNonValido
from bpmn4bpsim.bpsimclasses.ElementParameters import ElementParameters
from bpmn4bpsim.bpsimclasses.Modello import Modello
from bpmn4bpsim.schemas.ElementParameters import ElementParametersSC
from bpmn4bpsim.schemas.Model import ModelloSC


class ElementParametersCollectionAPI(Resource):
    """
    ElementParameters collection resource

    Methods
    -------
    get(self, idModello: str, idScenario: str):
        Return all ElementParameters of a scenario

    post(self, idModello: str, idScenario: str):
        Add an ElementParameters to a scenario

    delete(self, idModello: str, idScenario: str):
        Delete all element parameters from a scenario
    """

    @jwt_required()
    def get(self, idModello, idScenario):
        """
        GET Return a scenario's ElementParameters list

        Parameters
        ----------
        idModello: str
            model's id

        idScenario: str
            scenario's id
        """
        if mongo.db is None:
            return {"msg": "Impossible to connect to database"}, 500
        identity = get_jwt_identity()
        query = mongo.db.modelli.find_one(
            {"_id": ObjectId(idModello), "emailUtente": identity},
            {"scenari": {"$elemMatch": {"_id": idScenario}}},
        )
        if (
            not query
            or not query.get("scenari")
            or not query["scenari"][0].get("elementParameters")
        ):
            return {"msg": "Resource not found"}, 404

        return {
            "msg": "Ricerca riuscita",
            "elementParameters": query["scenari"][0]["elementParameters"],
        }

    @jwt_required()
    def post(self, idModello: str, idScenario: str):
        """
        POST add an ElementParameters element to the selected scenario

        Parameters
        ----------
        idModello: str
            model's id
        idScenario: str
            scenario's id
        elementParameter(body) : ElementParameters
            ElementParameter to add

        Returns
        -------
        JSON
            Metadata
        """
        # Check request
        if (
            request.content_length == 0
            or request.content_type != "application/json"
        ):
            return {"msg": "Atteso un oggetto JSON nel body."}, 400

        # retrieve data from request
        data = request.get_json()
        identity = get_jwt_identity()
        model_id_OID = ObjectId(idModello)

        # query db
        if mongo.db is None:
            return {"msg": "Impossible to connect to database"}, 500
        query = mongo.db.modelli.find_one(
            {"_id": model_id_OID, "emailUtente": identity},
            {"nomeModello": 1, "scenari": {"$elemMatch": {"_id": idScenario}}},
        )
        if not query or not query.get("scenari"):
            return {"msg": "Resource not found"}, 404

        # query fs
        modello_XML = GridFS(mongo.db).find_one({"_id": model_id_OID})
        if not modello_XML:
            return {
                "msg": "Internal error. Inconsistency between database and "
                "file system"
            }, 500

        # add element parameter
        try:
            modello_XML: etree._Element = etree.parse(modello_XML).getroot()
            query["_id"] = str(query["_id"])
            modello_schema = ModelloSC(modello_XML)
            modello: Modello = modello_schema.load(query)
            ep_schema = ElementParametersSC(modello_XML)
            ep: ElementParameters = ep_schema.load(data)
            elem_param_id = modello.addElementParameter(idScenario, ep)
        except ValidationError:
            return {"msg": "Error during validation"}, 400
        except Exception:
            return {"msg": "Uknown erro"}, 500

        update_q = mongo.db.modelli.update_one(
            {"_id": model_id_OID},
            {
                "$push": {
                    "scenari.$[scenario].elementParameters": ep_schema.dump(
                        modello.scenario(idScenario).elementParameter(
                            elem_param_id
                        )
                    )
                }
            },
            array_filters=[{"scenario._id": idScenario}],
        )
        if not update_q.modified_count:
            return {"msg": "Error while inserting into database"}, 500

        return {
            "msg": "Successful operation",
            "idElementParameter": elem_param_id,
        }

    @jwt_required()
    def delete(self, idModello, idScenario):
        """
        DELETE empty the list of ElementParameters of the selected scenario

        Parameters
        ----------
        idModello : str
            model's id
        idScenario : str
            scenario's id

        Returns
        -------
        Optional[application/json]
            Metadata
        """
        # retrieving data from request
        identity = get_jwt_identity()
        idModelloOID = ObjectId(idModello)

        # delete_query
        if mongo.db is None:
            return {"msg": "Impossible to connect to database"}, 500

        delete = mongo.db.modelli.update_one(
            {"_id": idModelloOID, "emailUtente": identity},
            {"$unset": {"scenari.$[scenario].elementParameters": ""}},
            array_filters=[{"scenario._id": idScenario}],
        )
        if not delete.matched_count:
            return {"msg": "Resource not found"}, 404
        if not delete.modified_count:
            return {
                "msg": "Error while deleting the ElementParameters' list"
            }, 500
        return None, 204


class ElementParametersAPI(Resource):
    """
    ElementParameters resource

    Methods
    -------
    get(self, idModello: str, idScenario: str, idElementParameter: str)
        return the requested ElementParameters

    put(self, idModello: str, idScenario: str, idElementParameter: str):
        update selected ElementParameters

    delete(self, idModello: str, idScenario: str, idElementParameter: str)
        delete selected ElementParameters
    """

    @jwt_required()
    def get(self, idModello: str, idScenario: str, idElementParameter: str):
        """
        GET return the specified ElementParameters

        Parameters
        ----------
        idModello : str
            model's id
        idScenario : str
            scenario's id
        idElementParameter : str
            ElementParameters' id

        Returns
        -------
        application/json
            Metadata + ElementParameters
        """
        # retrieve data from request
        identity = get_jwt_identity()
        modello_OID = ObjectId(idModello)
        if mongo.db is None:
            return {"msg": "Impossible to connect to database"}, 500
        query = mongo.db.modelli.find_one(
            filter={
                "_id": modello_OID,
                "emailUtente": identity,
                "scenari": {
                    "$elemMatch": {
                        "_id": idScenario,
                        "elementParameters": {
                            "$elemMatch": {"parametersId": idElementParameter}
                        },
                    }
                },
            },
            projection={
                "emailUtente": 0,
            },
        )

        if not query or not query.get("scenari"):
            return {"msg": "Resource not found"}, 404

        # retrieve data from mongo gridfs
        model_XML = GridFS(mongo.db).find_one(modello_OID)
        if not model_XML:
            return {
                "msg": "Internal error. Inconsistency between "
                "database and file system"
            }, 500

        # search effective ep
        try:
            query["_id"] = str(query["_id"])
            model_schema = ModelloSC(model_XML)
            modello: Modello = model_schema.load(query)
            ep = modello.scenario(idScenario).elementParameter(
                idElementParameter
            )
        except ValidationError:
            return {"msg": "Invalid ElementParameters"}, 400
        except ParamNonValido:
            return {"msg": "Internal error"}, 500
        except Exception:
            return {"msg": "Uknown error"}, 500

        ep_schema = ElementParametersSC(model_XML)
        return ep_schema.dump(ep)

    @jwt_required()
    def put(self, idModello: str, idScenario: str, idElementParameter: str):
        """
        PUT Update an existing ElementParameters

        Parameters
        ----------
        idModello : str
            model's id
        idScenario : str
            scenario's id
        idElementParameter : str
            ElementParameters' id

        Returns
        -------
        JSON
            Metadata
        """
        # check request
        if (
            request.content_type != "application/json"
            or request.content_length == 0
        ):
            return {"msg": "Empty body. Expected an ElementParameters"}, 400

        # retrieve data from requeste
        identity = get_jwt_identity()
        model_OID = ObjectId(idModello)

        # retrieve data from mongo
        if mongo.db is None:
            return {"msg": "Impossible to connect to database"}, 500
        model_q = mongo.db.modelli.find_one(
            filter={
                "emailUtente": identity,
                "_id": model_OID,
                "scenari": {
                    "$elemMatch": {
                        "_id": idScenario,
                        "elementParameters": {
                            "$elemMatch": {"parametersId": idElementParameter}
                        },
                    }
                },
            },
            projection={"emailUtente": 0},
        )
        if not model_q:
            return {"msg": "Resource not found"}, 404

        # retrieve data from mongo gridfs
        model_XML = GridFS(mongo.db).find_one(model_OID)
        if not model_XML:
            return {
                "msg": "Internal error. Inconsistency between "
                "database and file system"
            }, 500

        data = request.get_json()
        # de-serialize
        try:
            model_XML: etree._Element = etree.parse(model_XML).getroot()
            model_schema = ModelloSC(model_XML)
            model_q["_id"] = str(model_q["_id"])
            model: Modello = model_schema.load(model_q)
            ep_schema = ElementParametersSC(model_XML)
            data["parametersId"] = idElementParameter
            ep: ElementParameters = ep_schema.load(data)
            model.scenario(idScenario).updateElementParameter(ep)
        except ValidationError:
            return {
                "msg": "Error while validating given ElementParameters"
            }, 400
        except Exception:
            return {"msg": "Uknown error"}, 500

        # update
        update = mongo.db.modelli.update_one(
            filter={"_id": model_OID},
            update={
                "$set": {"scenari.$[sc].elementParameters.$[ep]": {**data}}
            },
            array_filters=[
                {"sc._id": idScenario},
                {"ep.parametersId": idElementParameter},
            ],
        )
        if not update.matched_count or not update.modified_count:
            return {"msg": "Uknown error while updating contents"}, 404

        return {
            "msg": "Successful ElementParameters' update",
            "elemetnParameter": ep_schema.dump(
                model.scenario(idScenario).elementParameter(idElementParameter)
            ),
        }

    @jwt_required()
    def delete(self, idModello: str, idScenario: str, idElementParameter: str):
        """
        DELETE delete a specific ElementParameters

        Parameters
        ----------
        idModello : `str`
            model's id

        idScenario : `str`
            scenarioìs id

        idElementParameter : `str`
            ElementParameters' id

        Returns
        -------
        Optional[application/json]
            Metadata
        """
        # retrieving data from requet
        identity = get_jwt_identity()
        idModelloOID = ObjectId(idModello)

        # perfrom delete_query
        if mongo.db is None:
            return {"msg": "Impossible to connect to database"}, 500
        delete = mongo.db.modelli.update_one(
            {"_id": idModelloOID, "emailUtente": identity},
            {
                "$pull": {
                    "scenari.$[scenario].elementParameters": {
                        "parametersId": idElementParameter
                    }
                }
            },
            array_filters=[{"scenario._id": idScenario}],
        )
        if not delete.matched_count:
            return {"msg": "Resource not found"}, 404
        if not delete.modified_count:
            return {
                "msg": "Uknown error occured while deleting the "
                "ElementParameters"
            }, 500
        return {}, 204
