from flask_restful import Resource, request
from flask_jwt_extended import jwt_required, get_jwt_identity, get_jwt
from flask_pymongo import GridFS
from bpsimaasapi import bcrypt
from bpsimaasapi.mongosetup import mongo
from bpsimaasapi.manage_token import del_token, create_permanent_token


class UnregisterdUser(Resource):
    """
    Unregistered user resource, they still havent registerd or they want to
    delete subscription

    Methods
    -------
    get()
        Simple greeting message
    post()
        Register new account
    delete()
        Deletion of user resource
    """

    def get(self):
        """
        Simple message to redirect to a correct endpoin
        """
        return {"msg": "Please use username and password to authenticate"}

    def post(self):
        """
        Register new accounts

        Expect JSON object inside body containing registration info: password
        and email

        Parameters
        ----------
        request.JSON
            {
                email: str,
                password: str
            }

        Returns
        -------
        JSON
            {
                msg: str, // metadata,
                email: str,
                _id: str: // _id field in database for registerd user
            }, 201 // successful subscription
        """
        # check request
        if request.content_length and request.content_length <= 0:
            return {
                "msg": "Empty request body. We need at least: username, "
                "password"
            }, 400
        if (
            request.content_type
            and "application/json" not in request.content_type
        ):
            return {"msg": "We request application/json Content-Type"}, 400

        # retrieve data from request
        data = request.get_json(force=True)
        if not data or ("email" not in data or "password" not in data):
            return {
                "msg": "We need email and password to register new account"
            }, 400

        # check if email is already used
        if mongo.db == None:
            return {"msg": "database connection error"}, 500
        if mongo.db.utenti.find_one({"email": data["email"]}):
            return {"msg": "User already reaistered with current email"}, 409

        # TODO: No security mechanism for password enumeration
        userData = {
            "email": data["email"],
            "password": bcrypt.generate_password_hash(data["password"]),
        }
        queryInsertUser = mongo.db.utenti.insert_one(userData)
        if queryInsertUser.acknowledged and queryInsertUser.inserted_id:
            return {
                "msg": "Successful subscription",
                "email": data["email"],
            }, 201

    def delete(self):
        """
        Method for deleting an account, providing email and password, requires
        entering username / email and password
        """
        # check request
        if request.content_length == 0:
            return {
                "msg": "Missing credential",
                "details": "Provide email and password to delete an account",
            }, 400
        if request.content_type != "application/json":
            return {
                "msg": "Wrong Content-Type",
                "details": "Required application/json",
            }, 400

        # retrieve data from request
        data = request.get_json()
        if (
            not data
            or "email" not in data.keys()
            or "password" not in data.keys()
        ):
            return {
                "msg": "Missing email or password, or both",
            }, 400

        # Check if user exist
        if mongo.db == None:
            return {"msg": "database connection error"}, 500
        usr_info = mongo.db.utenti.find_one(
            {
                "email": data["email"],
            },
            {"email": 1, "password": 1},
        )
        if not usr_info:
            return {"msg": "User not found"}, 410
        if not bcrypt.check_password_hash(
            usr_info["password"], data["password"]
        ):
            return {"msg": "Wrong password"}, 401

        # cancella l'utente dal database e tutto il 'suo' materiale
        usr_models = mongo.db.modelli.find({"emailUtente": data["email"]})
        if len(list(usr_models)) > 0:
            mongo.db.modelli.delete_many({"emailUtente": usr_info["email"]})
            for model in usr_models:
                GridFS(mongo).delete(model["id"])
        delete_usr = mongo.db.utenti.delete_many(
            filter={"email": data["email"]}
        )
        if not delete_usr.deleted_count:
            return {"msg": "Uknown error while deleting user"}, 500

        return {}, 204


class User(Resource):
    """
    Registered user resource

    Methods
    -------
    GET
        Look at own data
    POST
        Log in to system
    DELETE
        Log out
    PUT
        Change user data
    """

    @jwt_required()
    def get(self):
        """
        Look at own data except password (only email though)
        """
        if mongo.db == None:
            return {"msg": "database connection error"}, 500
        usr_data = mongo.db.utenti.find_one(
            {"email": get_jwt_identity()}, {"_id": 0, "email": 1}
        )
        if not usr_data:
            return {"msg": "User not found"}, 404
        return usr_data

    def post(self):
        """
        Log in to system, giving back access and refresh token

        Parameters
        ----------
        request.JSON
        ```
        {
            email: str,
            password: str
        }
        ```

        Returns
        -------
        JSON
        ```
        {
            access: jwt,
            refresh: jwt
        }
        ```
        """
        if request.content_length and request.content_length <= 0:
            return {"msg": "empty body"}, 400
        if not request.is_json:
            return {"msg": "Wrong Content-Type"}, 400

        data = request.get_json()
        if not data or not (
            "email" in data.keys() and "password" in data.keys()
        ):
            return {
                "msg": "Missing data to authenticate"
            }, 400

        if mongo.db == None:
            return {"msg": "database connection error"}, 500
        userInfo = mongo.db.utenti.find_one(
            {"email": data["email"]}, {"_id": 0, "email": 1, "password": 1}
        )
        if not userInfo:
            return {
                "msg": f'No user found with email {data["email"]}.'
            }, 404
        # controlla la validità della password
        if not bcrypt.check_password_hash(
            userInfo["password"], data["password"]
        ):
            # Password non valida
            return {"msg": "Wrong password"}, 400
        # genera jwt e restituiscilo
        return create_permanent_token(identity=userInfo["email"])

    @jwt_required()
    def delete(self):
        """
        Log out
        """
        jti = get_jwt()["jti"]
        identity = get_jwt_identity()
        loggedOut = del_token(jti, identity)
        if loggedOut:
            return {}, 204

        return {"msg": "Something went wrong. Imppossible to log out"}, 500

    # TODO Richiede jwt
    def put(self):
        """
        Metodo usato per modificare le credenziali?
        """
        pass
