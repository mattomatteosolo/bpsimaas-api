"""Module that contains the parser resource (a simple get on it)"""
from bpmn4bpsim.schemas.parseroutputs import PossibleSC
from bson import ObjectId
from flask_restful import Resource
from flask_jwt_extended import jwt_required, get_jwt_identity
from gridfs import GridFS
from lxml import etree
from bpmn4bpsim.parser.parser import parseBPMN
from bpsimaasapi.mongosetup import mongo


class ParserAPI(Resource):
    """
    Resource Parser

    Output
    ------
    JSON a list of object like the following
    {
        'xpathID': <string: id dell'elemento del modello>,
        'parameters':{
            'input':[
                {
                    'groupParameter': str,
                    'parameters': [ str, ... ]
                }
            ],
            'output':[
                {
                    'groupParameter': str,
                    'parameters': [
                        {
                            'parameterName': str,
                            'possibleOutputs': [ str, ...]
                        }
                    ]
                }
            ]
        }
    }
    """

    @jwt_required()
    def get(self, idModello):
        if mongo.db is None:
            return {"msg": "It's impossible to connect to database"}, 500
        gridfs = GridFS(mongo.db)
        if not mongo.db.modelli.find_one(
            {"_id": ObjectId(idModello), "emailUtente": get_jwt_identity()}
        ):
            return {
                "msg": f"{get_jwt_identity()} doesn't own a model with id "
                f"{idModello}"
            }, 401
        modello = gridfs.find_one(ObjectId(idModello))
        if not modello:
            return {
                "msg": "No model found in file system for "
                f"{get_jwt_identity()}"
            }, 400

        try:
            xmlModel = etree.parse(modello)
        except Exception:
            return {
                "msg": "Error while parsing the document, not a valid xml"
            }, 400

        try:
            parseResult = parseBPMN(xmlModel.getroot())
        except Exception:
            return {
                "msg": "Uknown error while parsing for parameterizable"
            }, 500

        schema = PossibleSC()
        return {
            "msg": "Successful parsing",
            "parameterizable": [schema.dump(i) for i in parseResult],
        }
