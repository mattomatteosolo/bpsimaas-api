from bson import ObjectId
from flask import make_response
from flask_jwt_extended import jwt_required, get_jwt_identity
from flask_restful import Resource, request
from flask_pymongo import GridFS
from lxml import etree
from marshmallow import ValidationError
from bpsimaasapi import mongo
from bpmn4bpsim.bpsimclasses import ParamNonValido
from bpmn4bpsim.schemas.Model import ModelloSC


class DownloadModello(Resource):
    """
    Download a model with given scenarios

    Methods
    -------
    get(idModello: str) -> application/bpmn+xml
        Given a list of scenario's id, this method will return a model
        parameterized with those scenarios if those exist.
    """

    @jwt_required()
    def post(self, idModello):
        """
        Given a list of scenario's id, this method will return a model
        parameterized with those scenarios if those exist.

        Parameters
        ----------
        idModello: str
            model id

        idScenari: List[str]
            Lista di id di scenari
                {
                    scenari: [
                        str, ...
                    ]
                }

        Returns
        -------
        bpmn+xml, 200
            parameterized model
        """
        # Check request data
        if request.content_type != "application/json" or (
            request.content_length and request.content_length < 1
        ):
            return {
                "msg": "Waiting for a list of scenario's with the follogin "
                "format:",
                "details": {"scenari": ["list", "of", "ids"]},
            }, 400

        # retrieve data from request
        data = request.get_json()
        if not isinstance(data["scenari"], list) and len(data) < 1:
            return {
                "msg": "Waiting for a list of scenario's with the follogin "
                "format:",
                "details": {"scenari": ["list", "of", "ids"]},
            }, 400

        data = data["scenari"]
        if any(not isinstance(x, str) for x in data):
            return {
                "msg": "Waiting for a list of scenario's with the follogin "
                "format:",
                "details": {"scenari": ["list", "of", "ids"]},
            }, 400

        identity = get_jwt_identity()
        idModelloOID = ObjectId(idModello)

        # query db
        if mongo.db is None:
            return {"msg": "Impossible to connect to database"}, 500

        query = mongo.db.modelli.find_one(
            {"_id": idModelloOID, "emailUtente": identity},
            projection={"emailUtente": 0},
        )
        if not query:
            return {"msg": "Resource not found"}, 404
        query["_id"] = idModello
        modello = GridFS(mongo.db).find_one(idModelloOID)
        if not modello:
            return {
                "msg": "Inconsistency between the database and the file system"
            }, 500

        # deserialize
        try:
            modelloXML = etree.parse(modello, parser=parser).getroot()
            schema = ModelloSC(modelloXML)
            modello = schema.load(query)
            response = make_response(
                etree.tostring(
                    modello.toXML(data), encoding="unicode", pretty_print=True
                )
            )
            response.mimetype = "application/bpmn+xml"
            return response
        except ParamNonValido:
            return {"msg": "Error during deserialization"}, 500
        except ValidationError:
            return {"msg": "Error during serialization"}, 400
        except Exception as e:
            from traceback import format_tb
            print(e.__class__.__name__)
            print(e)
            print("".join(format_tb(e.__traceback__)))
            return {"msg": "Uknown error"}, 500


parser = etree.XMLParser(remove_blank_text=True)
