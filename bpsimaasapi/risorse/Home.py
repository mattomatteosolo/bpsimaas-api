from flask_restful import Resource


class HomeRes(Resource):
    """
    No resource, just a get for root route, just a greeting
    """

    def get(self):
        return {"msg": "Welcome in BPSimaaS!"}
