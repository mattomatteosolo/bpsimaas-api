"""Modulo che contiene le classi per la gestione della risorsa calendara.
"""
from bson import ObjectId
from flask import request
from flask_jwt_extended import get_jwt_identity, jwt_required
from flask_restful import Resource
from flask_pymongo import GridFS
from lxml import etree
from marshmallow import ValidationError

from bpmn4bpsim.bpsimclasses.Modello import Modello
from bpmn4bpsim.bpsimclasses.Calendar import Calendar
from bpmn4bpsim.schemas.Model import ModelloSC
from bpmn4bpsim.schemas.Calendar import CalendarSC

from bpsimaasapi.mongosetup import mongo


class CalendarAPI(Resource):
    """Calendar resource

    Methods
    -------
    get(idModello, idScenario)

    post(idModello, idScenario, calendar)

    delete(idModello, idScenario)
    """

    @jwt_required()
    def get(self, idModello, idScenario):
        """
        This will return a list of calendar
        """

        # retrieve data from request
        identity = get_jwt_identity()

        # retireve data from mongo
        if mongo.db is None:
            return {"msg": "It's impossible to connect with database"}, 500

        calendars_q = mongo.db.modelli.find_one(
            filter={
                "_id": ObjectId(idModello),
                "emailUtente": identity,
                "scenari": {"$elemMatch": {"_id": idScenario}},
            },
            projection={
                "_id": 0,
                "scenari": {
                    "$elemMatch": {
                        "_id": idScenario,
                    }
                },
            },
        )

        # check mongo data
        if not calendars_q:
            return {"msg": "Resource not found"}, 404

        return {
            "calendars": []
            if not calendars_q["scenari"][0].get("calendars")
            else calendars_q["scenari"][0]["calendars"]
        }

    @jwt_required()
    def post(self, idModello, idScenario):
        """
        POST new calendar

        Parameter
        ---------
        idModello: str
            model's id

        idScenario: str
            scenario's id

        calendar: bpmn4bpsim.schemas.Calendar
            icalendar in json format as specified in
            bpmn4bpsim.schemas.CalendarSC
        """

        # check request
        if request.content_type != "application/json" or (
            request.content_length and not request.content_length > 0
        ):
            return {
                "msg": "Waiting for a JSON body containing a calendar"
            }, 400

        # retrieve data from request
        identity = get_jwt_identity()
        data = request.get_json()

        # retrieve data from mongo
        if mongo.db is None:
            return {"msg": "It's impossible to connect to database"}, 500
        modello_q = mongo.db.modelli.find_one(
            filter={
                "_id": ObjectId(idModello),
                "emailUtente": identity,
            },
            projection={"emailUtente": 0},
        )
        if not modello_q:
            return {
                "msg": f"{identity} doesn't own model with id {idModello}"
            }, 404

        # retrieve data from fs
        modello_XML = GridFS(mongo.db).find_one(
            filter={
                "_id": ObjectId(idModello),
            }
        )
        if not modello_XML:
            return {"msg": "Inconsistenza tra db e fs."}, 500

        # de-serialize
        try:
            modello_XML = etree.parse(modello_XML).getroot()
            model_sc = ModelloSC(modello=modello_XML)
            calendar_sc = CalendarSC()
            calendar: Calendar = calendar_sc.load(data)
            modello_q["_id"] = str(modello_q["_id"])
            modello: Modello = model_sc.load(modello_q)
            modello.scenario(idScenario).addCalendar(calendar)
        except ValidationError:
            return {
                "msg": "Error during validation, probably some corruption "
                "happened to the model"
            }, 400
        except ValueError:
            return {"msg": "Impossible to add the calendar"}, 400
        except Exception:
            return {"msg": "Uknown error"}, 500

        # mongo update
        update_q = mongo.db.modelli.update_one(
            filter={
                "_id": ObjectId(idModello),
                "emailUtente": identity,
                "scenari": {"$elemMatch": {"_id": idScenario}},
            },
            update={
                "$push": {
                    "scenari.$[sc].calendars": calendar_sc.dump(calendar)
                }
            },
            array_filters=[{"sc._id": idScenario}],
        )

        if not update_q.matched_count or not update_q.modified_count:
            return {"msg": "Inconsistency in database"}, 500

        return {
            "msg": "Successful calendar loading",
            "calenarId": calendar.id,
        }, 201

    @jwt_required()
    def delete(self, idModello, idScenario):
        """Delete all calendar for the specified scenario"""

        # remove from mongo
        if mongo.db is None:
            return {"msg": "It's impossible to connect to database"}, 500
        calendars_q = mongo.db.modelli.find_one_and_update(
            filter={
                "_id": ObjectId(idModello),
                "emailUtente": get_jwt_identity(),
                "scenari": {"$elemMatch": {"_id": idScenario}},
            },
            update={"$unset": {"scenari.$[sc].calendars": []}},
            array_filters=[{"sc._id": idScenario}],
        )

        if calendars_q is None:
            return {"msg": "Resource not found"}, 404

        return {}, 204


class LoadedCalendarAPI(Resource):
    """
    Calendar resource

    Methods
    -------
    GET
    DELETE
    PUT
    """

    @jwt_required()
    def get(self, idModello, idScenario, idCalendar):
        """
        Return a specific calendar given its "coordinates"

        Parameters
        ----------
        idModello: str
            model's id

        idScenario: str
            scenario's id

        idCalendar: str
            calendar's id
        """

        if mongo.db is None:
            return {"msg": "It's impossible to connect to database"}, 500
        calendar_q = mongo.db.modelli.find_one(
            filter={
                "_id": ObjectId(idModello),
                "emailUtente": get_jwt_identity(),
            },
            projection={
                "scenari": {
                    "$elemMatch": {
                        "_id": idScenario,
                        "calendars": {"$elemMatch": {"id": idCalendar}},
                    }
                }
            },
        )
        if (
            not calendar_q
            or not calendar_q["scenari"]
            or not calendar_q["scenari"][0]["calendars"]
        ):
            return {"msg": "Resource not found"}, 404

        return calendar_q["scenari"][0]["calendars"][0]

    @jwt_required()
    def put(self, idModello, idScenario, idCalendar):
        """
        Method used to update a calendar

        Parameters
        ----------
        idModello: str
            model's id

        idScenario: str
            scenario's id

        idCalendar: str
            calendar's id
        """

        # check request if request.content_type != 'application/json':
        if request.content_type != "application/json" or (
            request.content_length and not request.content_length > 0
        ):
            return {
                "msg": "Expected a body with a JSON serialized calendar"
            }, 400

        # retrieve data from request
        data = request.get_json()

        # retrieve data from mongo
        if mongo.db is None:
            return {"msg": "It's impossible to connect to database"}, 500
        model_q = mongo.db.modelli.find_one(
            filter={
                "_id": ObjectId(idModello),
                "emailUtente": get_jwt_identity(),
                "scenari": {
                    "$elemMatch": {
                        "_id": idScenario,
                        "calendars": {"$elemMatch": {"id": idCalendar}},
                    }
                },
            },
            projection={"emailUtente": 0},
        )
        if not model_q:
            return {"msg": "Resource not found"}, 404

        # retrieve data from fs
        modello_XML = GridFS(mongo.db).find_one({"_id": ObjectId(idModello)})
        if not modello_XML:
            return {"msg": f"Model {idModello} non found on file system."}, 500

        # validate aka de-serialize
        try:
            modello_XML = etree.parse(modello_XML).getroot()
            model_sc = ModelloSC(modello_XML)
            model_q["_id"] = str(model_q["_id"])
            modello: Modello = model_sc.load(model_q)
            calendar_sc = CalendarSC()
            calendar: Calendar = calendar_sc.load(data)
            idCalendar = idCalendar if calendar.id != idCalendar else None
            modello.scenario(idScenario).updateCalendar(
                calendar, idCalendar=idCalendar
            )
        except ValidationError:
            return {"msg": "Error during validation"}, 400
        except ValueError:
            return {"msg": "Error during update"}, 400
        except Exception:
            return {"msg": "Uknown error"}, 500

        update_q = mongo.db.modelli.update_one(
            filter={
                "_id": ObjectId(idModello),
                "emailUtente": get_jwt_identity(),
                "scenari": {"$elemMatch": {"_id": idScenario}},
            },
            update={
                "$set": {
                    "scenari.$[sc].calendars.$[c]": calendar_sc.dump(calendar)
                }
            },
            array_filters=[{"sc._id": idScenario}, {"c.id": idCalendar}],
        )
        if not update_q:
            return {"msg": "Uknown error"}, 500

        return {"msg": "Successful calendar update"}

    @jwt_required()
    def delete(self, idModello, idScenario, idCalendar):
        """
        DELETE delete a calendar given its id

        Parameters
        ----------
        idModello: str
            model's id

        idScenario: str
            scenario's id

        idCalendar: str
            calendar's id
        """

        # retrieve data from db
        if mongo.db is None:
            return {"msg": "It's impossible to connect with database"}, 500
        model_q = mongo.db.modelli.find_one(
            filter={
                "_id": ObjectId(idModello),
                "emailUtente": get_jwt_identity(),
                "scenari": {
                    "$elemMatch": {
                        "_id": idScenario,
                        "calendars": {"$elemMatch": {"id": idCalendar}},
                    }
                },
            },
            projection={"emailUtente": 0},
        )
        if not model_q:
            return {"msg": "Risorsa non trovata"}, 404

        # retrieve data frm fs
        model_XML = GridFS(mongo.db).find_one({"_id": ObjectId(idModello)})
        if not model_XML:
            return {
                "msg": f"Model with id {idModello} not found in file system."
            }, 500

        # de-serialize aka validate operation
        try:
            model_XML = etree.parse(model_XML).getroot()
            model_sc = ModelloSC(model_XML)
            model_q["_id"] = str(model_q["_id"])
            model: Modello = model_sc.load(model_q)
            model.scenario(idScenario).deleteCalendar(idCalendar)
        except (TypeError, ValueError):
            return {"msg": "Error during deletion"}, 500
        except ValidationError:
            return {"msg": "Error during validation"}, 500
        except Exception:
            return {"msg": "Uknown error"}, 500

        update_op = "$pull"
        if not model.scenario(idScenario).calendars:
            update_op = "$unset"
        # delete from mongo aka pull from the calendars array
        delete_q = mongo.db.modelli.update_one(
            filter={
                "_id": ObjectId(idModello),
                "emailUtente": get_jwt_identity(),
            },
            update={
                update_op: {"scenari.$[sc].calendars": {"id": idCalendar}}
            },
            array_filters=[{"sc._id": idScenario}],
        )
        if not delete_q:
            return {"msg": "Error during deletion from database"}, 500

        return {}, 204
