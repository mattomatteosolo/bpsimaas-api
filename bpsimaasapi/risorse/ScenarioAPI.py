"""
Model for Scenario resources

API
-----------
{
    _id = `str`,
    name = `str`, // required
    description = `str`,
    author = `str`,
    vendor = `str`,
    version = `str`,
    inherits = `str`,
    created = `datetime`,
    modified = `datetime`,
    scenarioParameters = `ScenarioParameters`,
    elementParameters = `List[ElementParameters]`,
    vendorExtensions = `List[VendorExtensions]`,
    calendars = `List[Calendars]`
}

Classes
-------
ScenarioAPI
    Scenario Collection Resource

ScenarioSpecificoAPI
    Scenario Resource
"""
from bson import ObjectId
from flask_jwt_extended import get_jwt_identity, jwt_required
from flask_restful import Resource, request
from flask_pymongo import GridFS
from lxml import etree
from marshmallow import ValidationError

from bpsimaasapi import mongo
from bpmn4bpsim.bpsimclasses.Modello import Modello
from bpmn4bpsim.bpsimclasses import ParamNonValido
from bpmn4bpsim.bpsimclasses.Scenario import Scenario
from bpmn4bpsim.schemas.Model import ModelloSC
from bpmn4bpsim.schemas.Scenario import ScenarioSC


class ScenarioCollectionAPI(Resource):
    """
    Scenario Collection Resource

    Methods
    -------
    get(idModello: str)
        return the collection of scenario of the selected model

    post(idModello: str, data: request.data)
        add a scenario to a specific model
    """

    @jwt_required()
    def get(self, idModello):
        """
        GET return a list of scenario of a given model

        Parameters
        ----------
        idModello : str
            model's id

        Return
        ------
        application/json
            List of scenarios' basic information
        """
        if mongo.db is None:
            return {"msg": "It's impossible to connect to database"}, 500
        query = mongo.db.modelli.find_one(
            filter={
                "_id": ObjectId(idModello),
                "emailUtente": get_jwt_identity(),
            },
            projection={"nomeModello": 1, "scenari._id": 1, "scenari.name": 1},
        )
        if query:
            return {
                "_id": str(query["_id"]),
                "nomeModello": query["nomeModello"],
                "scenari": query["scenari"],
            }
        # Default in cui non esiste il modello o non è in possesso dell'utente
        # che fornisce il jwt
        return {"msg": "No resource found"}, 404

    @jwt_required()
    def post(self, idModello: str):
        """
        POST Add a scenario

        Parameters
        ----------
        idModello : str
            model's id

        scenario(body): Scenario


        Return
        ------
        application/json
            Metadata
        """

        # check request
        if (
            request.content_length and not request.content_length > 0
        ) or request.content_type != "application/json":
            return {"msg": "Invalid body. Waiting JSON data."}, 400

        # retrieve data from request
        data = request.get_json()
        identity = get_jwt_identity()

        # retrieve data from mogno
        if mongo.db is None:
            return {"msg": "It's impossible to connect to database"}, 500
        modello_q = mongo.db.modelli.find_one(
            filter={"_id": ObjectId(idModello), "emailUtente": identity},
            projection={"emailUtente": 0},
        )
        if not modello_q:
            return {
                "msg": f"No model {idModello} found for {get_jwt_identity()}"
            }, 400

        modello_XML = GridFS(mongo.db).find_one({"_id": ObjectId(idModello)})

        try:
            modello_XML = etree.parse(
                modello_XML, parser=parserNoSpace
            ).getroot()
            model_schema = ModelloSC(modello_XML)
            modello_q["_id"] = str(modello_q["_id"])
            model: Modello = model_schema.load(modello_q)
            scenario_schema = ScenarioSC(modello_XML)
            scenario: Scenario = scenario_schema.load(data)
            model.addScenario(scenario)
        except ValidationError:
            return {
                "msg": "Input data validation error. Verify Your input."
            }, 400
        except Exception:
            return {"msg": "Uknown error"}, 500

        # Se non ho lanicato eccezioni, significa che posso aggiungere i dati
        # passati alla collezione modelli
        query = mongo.db.modelli.update_one(
            filter={
                "_id": ObjectId(idModello),
                "emailUtente": get_jwt_identity(),
            },
            update={"$push": {"scenari": scenario_schema.dump(scenario)}},
        )
        if query.modified_count > 0:
            return {
                "msg": "Scenario added succesfully",
                "scenario": {"_id": scenario._id, "name": scenario.name},
            }
        return {"msg": "Error adding scenario"}, 500


parserNoSpace = etree.XMLParser(remove_blank_text=True)


class ScenarioAPI(Resource):
    """
    Scenario resource

    Methods
    -------
    get(idModello: str, idScenario: str)
        Retrun a scenario

    put(idModello: str, idScenario: str)
        Update a scenario

    delete(idModello: str, idScenario: str)
        Delete a scenario
    """

    @jwt_required()
    def get(self, idModello, idScenario):
        """
        GET Return selected scenario

        Parameters
        ----------
        idModello : str
            model's id

        idScenario : str
            scenario's id
        """
        if mongo.db is None:
            return {"msg": "It's impossible to connect to database"}, 500

        query = mongo.db.modelli.find_one(
            {"_id": ObjectId(idModello), "emailUtente": get_jwt_identity()},
            {"_id": 0, "scenari": {"$elemMatch": {"_id": idScenario}}},
        )

        if (
            not query
            or not query.get("scenari")
            or len(query.get("scenari")) == 0
        ):
            return {"msg": "Resource not found"}, 404
        return {"scenario": query["scenari"][0]}

    @jwt_required()
    def put(self, idModello: str, idScenario: str):
        """
        PUT Update selected scenario with given data

        Parameters
        ----------
        idModello : str
            model's id

        idScenario : str
            scenario's id

        scenario : JSON
            data of the updated scenario
        """

        if request.content_type != "application/json":
            return {"msg": "No data was found. Expected JSON data"}, 400
        data = request.get_json(force=True)
        if not data:
            return {"msg": "No data was found. Expected JSON data"}, 400

        if mongo.db is None:
            return {"msg": "It's impossible to connect to database"}, 500

        queryModello = mongo.db.modelli.find_one(
            {"_id": ObjectId(idModello), "emailUtente": get_jwt_identity()},
            projection={"emailUtente": 0},
        )
        if not queryModello:
            return {
                "msg": f"No model {idModello} found for {get_jwt_identity()}"
            }, 404

        fs = GridFS(mongo.db)
        modello_XML = fs.find_one({"_id": ObjectId(idModello)})
        modello_XML = etree.parse(modello_XML).getroot()
        try:
            queryModello["_id"] = str(queryModello["_id"])
            model_schema = ModelloSC(modello_XML)
            model: Modello = model_schema.load(queryModello)
            if data["_id"] != idScenario:
                model.updateIdScenario(idScenario, data["_id"])
            scenario_schema = ScenarioSC(modello_XML)
            nuovo_scenario: Scenario = scenario_schema.load(data)
            print("Il nuovo scenario non dispiace")
            model.updateScenario(nuovo_scenario)
        except ValidationError:
            return {"msg": "Error during validation"}, 400
        except Exception:
            return {"msg": "Uknown error"}, 500

        delete = mongo.db.modelli.update_one(
            {"_id": ObjectId(idModello)},
            {"$pull": {"scenari": {"_id": idScenario}}},
        )
        if not delete:
            return {"msg": "Impossible to update the scenario"}, 400

        # 2, aggiungere lo scenario
        add = mongo.db.modelli.update_one(
            {"_id": ObjectId(idModello)},
            {"$push": {"scenari": scenario_schema.dump(nuovo_scenario)}},
        )
        if not add:
            return {"msg": "Impossible to update the scenario"}, 400

        return {"msg": "Scenario updated succesfully"}

    @jwt_required()
    def delete(self, idModello: str, idScenario: str):
        """
        DELETE delete the seleceted scenario

        Parameters
        ----------
        idModello : str
            model's id

        idScenario : str
            scenario's id
        """
        if mongo.db is None:
            return {"msg": "It's impossible to connect to database"}, 500

        identity = get_jwt_identity()
        modello_q = mongo.db.modelli.find_one(
            {"_id": ObjectId(idModello), "emailUtente": identity},
            projection={"emailUtente": 0},
        )
        if not modello_q:
            return {
                "msg": f"No model {idModello} found for {get_jwt_identity()}"
            }, 404

        fs = GridFS(mongo.db)
        modello_XML = fs.find_one({"_id": ObjectId(idModello)})
        if not modello_XML:
            return {
                "msg": "Internal error. Inconsistency between database and "
                "file system."
            }, 500

        # controllo che il modello si possa rimuovere
        try:
            modello_XML = etree.parse(modello_XML).getroot()
            modello_q["_id"] = str(modello_q.pop("_id"))
            model_schema = ModelloSC(modello_XML)
            modello: Modello = model_schema.load(modello_q)
            modello.removeScenario(idScenario)
        except ParamNonValido:
            return {"msg": "Error while removing scenario"}, 500
        except Exception:
            return {"msg": "Uknown error"}, 500

        # rimuvo lo scenario da mongo
        delete_q = mongo.db.modelli.update_one(
            {"_id": ObjectId(idModello)},
            {"$pull": {"scenari": {"_id": idScenario}}},
        )
        if delete_q.modified_count > 0:
            return {"msg": "Scenario removed correctly", "_id": idScenario}

        return {"msg": "Uknown error. Impossible to remove scenario."}, 500
