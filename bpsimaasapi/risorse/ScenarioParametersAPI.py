"""
Module for ScenarioParameters resource

API
-----------
{
    start: Parameter,
    duration: Parameter,
    warmup: Parameter,
    propertyParameters: List[PropertyParameters],
    baseResultFrequency: string,
    seed: int,
    replication: int,
    baseTimeUnit: TimeUnit (as string),
    baseCurrencyUnit: Currency (as string),
    baseResultFrequencyCumul: bool,
    traceOutput: bool,
    traceFormat: str
}
"""
from bson import ObjectId
from flask_jwt_extended import get_jwt_identity, jwt_required
from flask_restful import Resource, request
from flask_pymongo import GridFS

from lxml import etree
from bpsimaasapi import mongo
from bpmn4bpsim.bpsimclasses.Modello import Modello
from bpmn4bpsim.bpsimclasses.ScenarioParameters import ScenarioParameters
from bpmn4bpsim.schemas.Model import ModelloSC
from bpmn4bpsim.schemas.ScenarioParameters import ScenarioParametersSC


class ScenarioParametersAPI(Resource):
    """
    ScenarioParameters resource

    Methods
    -------

    post(idModello: str, idScenario: str)
        add ScenarioParameters

    get(idModello: str, idScenario: str)
        return ScenarioParameters

    put(idModello: str, idScenario: str)
        update ScenarioParameters

    delete(idModello: str, idScenario: str)
        delete ScenarioParameters
    """

    @jwt_required()
    def get(self, idModello: str, idScenario: str):
        """
        GET return ScenarioParameters

        Parameters
        ----------

        idModello: str
            model's id

        idScenario: str
            scenario's id
        """
        if mongo.db is None:
            return {"msg": "It's impossible to connect to database"}, 500

        identity = get_jwt_identity()
        query = mongo.db.modelli.find_one(
            {"_id": ObjectId(idModello), "emailUtente": identity},
            {"scenari": {"$elemMatch": {"_id": idScenario}}},
        )
        if (
            query is None
            or query.get("scenari") is None
            or len(query["scenari"]) == 0
            or query["scenari"][0].get("scenarioParameters") is None
        ):
            return {"msg": "Resource not found"}, 404

        return {
            "ScenarioParameters": query["scenari"][0].get(
                "scenarioParameters"
            ),
        }

    @jwt_required()
    def post(self, idModello: str, idScenario: str):
        """
        POST add scenario parameters to a scenario

        Parameters
        ----------

        idModello: str
            model's id

        idScenario: str
            scenario's id
        """
        # Check request
        if (
            request.content_length and not request.content_length > 0
        ) or request.content_type != "application/json":
            return {
                "msg": "Invalid request. Expect ScenarioParameters as JSON "
                + "data"
            }, 400

        if mongo.db is None:
            return {"msg": "It's impossible to connect to database"}, 500

        # retrieve data from request
        sc_parameters_JSON = request.get_json()
        identity = get_jwt_identity()

        # retrieve data from mongo
        query = mongo.db.modelli.find_one(
            {"_id": ObjectId(idModello), "emailUtente": identity},
            {"nomeModello": 1, "scenari": {"$elemMatch": {"_id": idScenario}}},
        )
        if not query or not query.get("scenari") or len(query["scenari"]) < 1:
            return {"msg": "Resource not found"}, 404

        if query["scenari"][0].get("scenarioParameters") is None:
            return {
                "msg": "ScenarioParameters already exists for scenario "
                + f"{idScenario}",
            }, 400

        # retrieve data from mongo gridfs
        modello_XML = GridFS(mongo.db).find_one({"_id": ObjectId(idModello)})
        if not modello_XML:
            return {
                "msg": "Internal error. Inconsistency between database and "
                "file system"
            }, 500

        # Controlla che l'oggetto sia un valido scenario parameters
        # e che si possa aggiungere allo scenario selezionato
        # Quindi, de-serializzazione modello
        try:
            query["_id"] = str(query["_id"])
            modello_schema = ModelloSC(etree.parse(modello_XML).getroot())
            modello: Modello = modello_schema.load(query)
            scenario_params_schema = ScenarioParametersSC()
            sc_parameters: ScenarioParameters = scenario_params_schema.load(
                sc_parameters_JSON
            )
            scenario = modello.scenario(idScenario)
            if scenario is None:
                return {"msg": "Scenario not foun"}, 404
            scenario.scenarioParameters = sc_parameters
        except Exception:
            return {"msg": "Uknown Error"}, 500

        # update data
        update = mongo.db.modelli.update_one(
            {"_id": ObjectId(idModello), "emailUtente": identity},
            {
                "$set": {
                    "scenari.$[element].scenarioParameters": sc_parameters_JSON
                }
            },
            array_filters=[{"element._id": idScenario}],
        )
        if not update.modified_count == 1:
            return {"msg": "Uknown error"}, 500

        return {
            "msg": "Successful insertion",
            "scenarioParameters": scenario_params_schema.dump(sc_parameters),
        }

    @jwt_required()
    def put(self, idModello: str, idScenario: str):
        """
        PUT update ScenarioParameters for a given scenario

        Parameters
        ----------

        idModello: str
            model's id

        idScenario: str
            scenario's id
        """
        if request.content_length and (
            not request.content_length > 0
            or request.content_type != "application/json"
        ):
            return {
                "msg": "Invalid request. Expect ScenarioParameters as JSON "
                + "data"
            }, 400
        sc_parameters_JSON = request.get_json()
        identity = get_jwt_identity()

        # retrieving data from mongo
        if mongo.db is None:
            return {"msg": "It's impossible to connect to database"}, 500
        query = mongo.db.modelli.find_one(
            {"_id": ObjectId(idModello), "emailUtente": identity},
            {"nomeModello": 1, "scenari": {"$elemMatch": {"_id": idScenario}}},
        )
        if (
            not query
            or not query.get("scenari")
            or len(query["scenari"]) < 1
            or not query["scenari"][0].get("scenarioParameters")
        ):
            return {"msg": "Resource not found"}, 404

        # retrieving data from mongo gridfs
        modello_XML = GridFS(mongo.db).find_one({"_id": ObjectId(idModello)})
        if not modello_XML:
            return {
                "msg": "Internal error. Inconsistency between database and "
                + "file system"
            }, 500

        # De-serializzazione modello
        try:
            query["_id"] = str(query["_id"])
            modello_schema = ModelloSC(etree.parse(modello_XML).getroot())
            modello: Modello = modello_schema.load(query)
            scenario_params_schema = ScenarioParametersSC()
            sc_parameters: ScenarioParameters = scenario_params_schema.load(
                sc_parameters_JSON
            )
            modello.scenario(idScenario).scenarioParameters = sc_parameters
        except Exception:
            return {
                "msg": "Uknown error",
            }, 500

        # update
        update = mongo.db.modelli.update_one(
            {"_id": ObjectId(idModello), "emailUtente": identity},
            {
                "$set": {
                    "scenari.$[element].scenarioParameters": sc_parameters_JSON
                }
            },
            array_filters=[{"element._id": idScenario}],
        )
        if not update.modified_count == 1:
            return {"msg": "Uknown error"}, 500

        return {
            "msg": "ScenarioParameters updated successfully",
            "scenarioParameters": scenario_params_schema.dump(sc_parameters),
        }

    @jwt_required()
    def delete(self, idModello: str, idScenario: str):
        """
        DELETE delete ScenarioParameters for a given Scenario

        Parameters
        ----------

        idModello: str
            model's id

        idScenario: str
            scenario's id
        """
        if mongo.db is None:
            return {"msg": "It's impossible to connect to database"}, 500

        query = mongo.db.modelli.update_one(
            {
                "_id": ObjectId(idModello),
                "emailUtente": get_jwt_identity(),
                "scenari": {
                    "$elemMatch": {
                        "_id": idScenario,
                    }
                },
            },
            {"$unset": {"scenari.$[element].scenarioParameters": ""}},
            array_filters=[{"element._id": idScenario}],
        )
        if query.matched_count != 1 or query.modified_count != 1:
            return {
                "msg": "Uknown error during deletion of ScenarioParameters"
            }, 404

        return {}, 204
