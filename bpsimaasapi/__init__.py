import json
import os

from flask import Flask
from bpsimaasapi.cors import cors
from bpsimaasapi.mongosetup import mongo
from bpsimaasapi.bpcrypt import bcrypt
from bpsimaasapi.jwt import jwt
from bpsimaasapi.api import api

app = Flask(__name__)

UpDir = os.path.split(os.path.curdir)[0]
with open(os.path.join(UpDir, '.config.json')) as ConfigFile:
    configs = json.load(ConfigFile)

if 'SECRET_KEY' in configs.keys():
    app.secret_key = configs['SECRET_KEY']
    app.config['JWT_SECRE_KEY'] = configs['SECRET_KEY']

if 'mongo' in configs.keys():
    mongoUser = configs['mongo']['username']
    mongoPassword = configs['mongo']['password']
    mongoHost = configs['mongo']['host']
    mongoPort = configs['mongo']['port']
else:
    raise KeyError("No mongo confis have been found")

app.config['MONGO_URI'] = f"mongodb://{mongoUser}:{mongoPassword}@{mongoHost}:{mongoPort}/BPSimaaS?authSource=BPSimaaS"
app.config['JWT_BLACKLIST_ENABLED'] = True
app.config['SCHEMA_LOCATION'] = 'BPSimaaSAPI/schema'

mongo.init_app(app)
if mongo.db is None:
    raise ConnectionError("Impossible to connect to mongo db")
cors.init_app(app)
jwt.init_app(app)
api.init_app(app)
bcrypt.init_app(app)

