from flask_jwt_extended import create_access_token, create_refresh_token
from flask_pymongo.wrappers import Database
from typing import cast

from bpsimaasapi.mongosetup import mongo


def create_permanent_token(identity, claims=None):
    '''
    Wrapper function around jwt create_access_token that is used to create a
    permanent jwt access and refresh token

    Parameters
    ----------
    identity : something that is JSON serializable
        pass 
    claims : something that is JSON serializable
        pass

    Returns
    -------
    token : jwt access token
        pass
    '''
    return {
        "access": create_access_token(
            identity=identity,
            expires_delta=False,
            additional_claims=claims),
        "refresh": create_refresh_token(
            identity=identity,
            expires_delta=False,
            additional_claims=claims)
    }


def del_token(jwti, identity):
    '''
    Function used to insert jwt inside a per user blocklist

    Parameters
    ----------
    jwti : 
        jti of a jwt
    identity : 
        identity of a jwt

    Returns
    -------
    blacklisted : bool
        true if inserted inside blocklist correctly, false otherwise
    '''
    db = cast(Database, mongo.db)
    q = db.utenti.update_one(
        filter={
            'email': identity
        },
        update={
            '$push': {
                'blacklist': jwti
            }
        }
    )
    if q.acknowledged and q.modified_count > 0:
        return True
    return False
