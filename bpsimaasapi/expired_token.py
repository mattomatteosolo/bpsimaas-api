from bpsimaasapi.mongosetup import mongo
from bpsimaasapi.jwt import jwt


@jwt.token_in_blocklist_loader
def check_token_in_blacklist(_, jwt_data) -> bool:
    '''
    This function will check for ivalid token inside our database, since we are
    not yet providing a mechanism for refreshing token and only produce long
    term jwt

    Parameters
    ----------
    jwt_data
        jwt payload
    '''
    jti = jwt_data['jti']
    email = jwt_data['sub']
    if mongo.db is None:
        raise ConnectionError("")
    utenti = mongo.db.get_collection("utenti")
    q = utenti.find_one(
        {
            'email': email
        },
        {
            '_id': 0,
            'password': 0
        }
    )
    if not q:
        return True
    return 'blacklist' in q.keys() and jti in q['blacklist']
