from bpsimaasapi.risorse.Home import HomeRes
from bpsimaasapi.risorse.Access import UnregisterdUser, User
from bpsimaasapi.risorse.ModelAPI import ModelAPI, LoadedModelAPI
from bpsimaasapi.risorse.ParserAPI import ParserAPI
from bpsimaasapi.risorse.ScenarioAPI import ScenarioCollectionAPI, ScenarioAPI
from bpsimaasapi.risorse.ScenarioParametersAPI import ScenarioParametersAPI
from bpsimaasapi.risorse.ElementParametersAPI import (
    ElementParametersCollectionAPI,
    ElementParametersAPI,
)
from bpsimaasapi.risorse.Download import DownloadModello
from bpsimaasapi.risorse.Calendar import CalendarAPI, LoadedCalendarAPI

from flask_restful import Api

# Flask-restfull setup
api = Api()
apis = [
    (HomeRes, "/"),
    (UnregisterdUser, "/register"),
    (User, "/login"),
    (ModelAPI, "/models"),
    (LoadedModelAPI, "/models/<idModello>"),
    (ParserAPI, "/models/<idModello>/parser"),
    (DownloadModello, "/models/<idModello>/download"),
    (ScenarioCollectionAPI, "/models/<idModello>/scenario"),
    (CalendarAPI, "/models/<idModello>/scenario/<idScenario>/calendar"),
    (
        LoadedCalendarAPI,
        "/models/<idModello>/scenario/<idScenario>/calendar/<idCalendar>",
    ),
    (ScenarioAPI, "/models/<idModello>/scenario/<idScenario>"),
    (
        ScenarioParametersAPI,
        "/models/<idModello>/scenario/<idScenario>/scenarioparameters",
    ),
    (
        ElementParametersCollectionAPI,
        "/models/<idModello>/scenario/<idScenario>/elementparameters",
    ),
    (
        ElementParametersAPI,
        "/models/<idModello>/scenario/<idScenario>/elementparameters/"
        "<idElementParameter>",
    ),
]
for res, path in apis:
    api.add_resource(res, path)
