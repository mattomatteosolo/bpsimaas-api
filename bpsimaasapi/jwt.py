from flask_jwt_extended import JWTManager
# jwt machinery setup
jwt = JWTManager()
from bpsimaasapi.expired_token import check_token_in_blacklist
