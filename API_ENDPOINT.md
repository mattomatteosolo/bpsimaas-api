# API

The api in this project are REST WEB API that uses JSON as data format. So, it
is required that every request to them set "Content-type" header to
"application/json".

To authenticate a user, this APIs make use of [JWT](https://jwt.io/). The only
resources that do not use them are those realated to authentication, like login
and some methods of register.

# Resources

-   [register](#register)
-   [login](#login)
-   [models](#models)
-   [specific model](#specific-model)
-   [parser](#parser)
-   [scenario](#scenario)
    -   [scenario collections](#scenario-collections)
    -   [specific scenario](#common-scenario)
-   [scenario parameters](#scenario-parameters)
-   [element parameters](#element-parameters)
    -   [element parameters collections](#element-parameters-collection)
    -   [single element parameters](#single-element-parameters)
-   [download](#download)

## Register {#register}

URL: /register

Methods:

### GET:

Output:

-   200, Helper message

### POST

Account registration

Input: user data used for later acces, for now, email and password 

eg:

```JSON
{
    "username": "test",
    "password": "test"
}
```

Output:

-   201, Registration successful
-   409, Credential already in use

### DELETE

Delete an account

Input: email and password

eg:

```JSON
{
    "username": "test",
    "password": "test"
}
```

Output:

-   204, Account deletion successful
-   401, Incorrect password
-   410, There's no user with given email

## Login {#login}

URL: /login

### GET

Retrieve user data except password (for now only email)

Output:

-   200, User data
-   404, User not found

### POST

Log in method (this will return a JWT)

Input: username/email and password

eg:

```JSON
{
    "email": "test@test.com",
    "password": "test"
}
```
Output:

-   404, if no user with given email has been found
-   200, JWT acces and refresh token

    eg:

    ```JSON
    {
        "access": "XXXX.YYYYYYYY.ZZZZZZZZ",
        "refresh": "dsasd.dsadasd.asdasd"
    }
    ```

### DELETE

Log out method ("delete" JWT given as "Bearer-token" )

Output:

-   204, Log out successful
-   500, Impossible to log out from the given JWT, probably already logged
    out

From this point on, every shown endpoint needs that authentication is set to
Bearer and that a JWT is provided.

## Models {#models}

URL: /models

Methods:

### GET

Output:

-   200, List of BPMN loaded models

    eg:

    ```JSON
    {
        "modelli": [
            {
                "_id": "<id>",
                "nomdeModello": "<filename>"
            }
            ...
        ]
    }
    ```

### POST

Adds the model provided to the body as from-data with name "modello"

Input: BPMN model. Yells if a model with the same name already exist for the
current user.

Output:

-   200, information for the loaded module

    ```JSON
    {
        "msg": "Modello aggiunto correttamente",
        "inserted_id": "<id>",
        "nomeMoello": "<modello-filename>"
    }
    ```
From now on, <modelId\> refers to a BPMN model already deployed.

## Specific model {#specific-model}

URL: /models/\<modelId\>  

Methods:

### GET

This method will return a BPMN model (corresponding to the one with id modelId)
in xml format, without any extension.

Output:

-   200, Content-type: "application/bpmn+xml"
-   404, No model found with id modelId
-   500, Discrepancy between filesystem and database

### PUT

Update model

Output:

-   500, errors
-   200, update successful

### DELETE

Delete model with id modelId owned by the owner of the JWT

Output:

-   204, deletion successful
-   404, user that owns the JWT doesn't own a model with given id
-   500, internal errors

## Parser {#parser}
/models/<modelId\>/parser

### GET

Query the parser for the current (modelId) model

Output:,

eg:

```JSON
{
    ...some metadata...,
    "parameterizable":[
        {
            "xpathID": str,
            "input":[
                {
                    "groupParameter": str,
                    "parameters":[
                        str,
                        ...
                    ]
                },
                ...
            ],
            "output":[
                {
                    "groupParameter": str,
                    "parameters":[
                        {
                            "parameterName": str,
                            "possibleOutputs":[
                                str,
                                ...
                            ]
                        }
                    ]
                },
                ...
            ],
        },
        ...
    ],
}
```

## Scenario {#scenario}

This resource actually span across two endpoints: Scenario Collenctions and
Common Scenario.

## Scenario Collenctions {#scenario-collections}

URL: /models/\<modelId\>/scenario 

Methods:

### GET

Output: list of names and ids of scenarios

### POST

Input:scenario as defined in bpmn4bpsim.schemas.Scenario

```JSON
{
    _id = `str`,
    name = `str`, // required
    description = `str`,
    author = `str`,
    vendor = `str`,
    version = `str`,
    inherits = `str`,
    created = `datetime`,
    modified = `datetime`,
    scenarioParameters = `ScenarioParameters`,
    elementParameters = `List[ElementParameters]`,
    vendorExtensions = `List[VendorExtensions]`,
    calendars = `List[Calendars]`
}
```

Output: "personal data" of the just registered scenario

## Specific Scenario {#common-scenario}

URL: /models/\<modelId\>/scenario/\<scenarioId\>

Methods:

### GET

Output:

-   200, scenario with id scenarioId 
-   404, if user owning the given JWT doesn't own a scenario with such id

### PUT

Input:scenario such as specified in bpsimclasses.schemas.Scenario

Output:

-   200, 
-   404, if the user doesn't own the resources

### DELETE

Delete a scenario given its id

Output: 
-   200

    ```JSON
    {
        "msg": "...",
        "_id": "<id-scenario>"
    }
    ```
-   404, no such resources owned by the user

## ScenarioParameters {#scenario-parameters}

URL: /models/\<modelId\>/scenario/\<scenarioId\>/scenarioparameters

Methods:

### GET

Retrieve ScenarioParameters for the given scenario

Output:

-   200,
-   404, no such reources for current user, or model, or scenario

### POST

Method used to add a ScenarioParameters for the specified scenario

Input: ScenarioParameters as defined in bpmn4bpsim.schema.ScenarioParameters

```JSON
{
    "start": `Parameter`,
    "duration" = `Parameter`,
    "warmup" = `Parameter`,
    "propertyParameters" = `PropertyParameters`,
    "baseResultFrequency" = `str`,
    "seed" = `str`,
    "replication" = `int`, // > 0
    // one of ['ms','s','min','hour','day','year']
    "baseTimeUnit" = `TimeUnit`,
    "baseCurrencyUnit" = `str`,
    "baseResultFrequencyCumul" = `bool`,
    "traceOutput" = `bool`,
    "traceFormat" = `str`
}
```

Output:

-   200

    ```JSON
    {
        "msg": "...",
        "scenarioParameters": <scenazio-parameters>
    }
    ```

-   404, no such scenario (scenarioId) owned by the user/model
-   500, internal error. Or scenario already exists: use PUT instead

### PUT

Modify a ScenarioParameters

Input: Scenario such as bpmn4bpsim.schema.ScenarioParameters

Output:,
-   200

    ```JSON
    {
        "msg": "...",
        "scenarioParameters": <updated-scenario>
    }
    ```

-   404, no such ScenarioParameters

### DELETE

Delete ScenarioParameters for the given scenario

Output:

-   204, deletion successful
-   404, no such resources model/scenario/scenario parameters

## ElementParameters {#element-parameters}

This resource, as per Scenario, uses two different endpoint: one to handle the
ElementParameters collection, and one to handle the single ElementParameters.

## ElementParameters {#element-parameters-collection}
URL: /models/\<modelId\>/scenario/\<scenarioId\>/elementparameters

Methods:

### GET

Used to retrieve ElementParameters of a particular scenario

Output:

-   200, list of requested ElementParameters
-   404, no such resources for user/model/scenario

### POST

This metod will be used to add an ElementParameter to the selected
Scenario

Input: JSON object as specified in schemas.ElementParameter

```JSON
{
    "elementRef": `str`, // required
    "parametersId": `str`,
    "parametersGroup": `List[ParametersGroup]`,
    "vendorExtensions": `List[VendorExtentions]`
}

```

Output:

-   200, successful operation
-   404, no such resources for user/model/scenario

### DELETE

Call this endpoint with this method to delete all ElementParameters of a given
scenario

Output:

-   204, successful deletion of ElementParameters
-   404, no such resources user/model/scenario/element parameters

## Single ElementParameters {#single-element-parameters}

URL: /models/<modelId\>/scenario/<scenarioId\>/elementparameters/<idElementParameter\>

Methods:

### GET

This method return an ElementParameter given its "coordinates"

Output:

-   200, research went fine and the selected ElementParameter is returned
-   404, no such ElementParameter was found

### PUT

Update an ElementParameter

Input: ElementParameter as defined in schem/ElementParameter

Output:

-   200, successful update
-   404, resource to update not found

### DELETE

Delete a specific ElementParameter

Output:

-   204, successful deletion
-   404, no such resource at given "coordinates"

## Download {#download}

URL: /models/<modelId\>/download

Methods:

### POST

Given a list of scenarios' ids will return a model with those scenarios attached

Input: List[str], scenarios' id
-   200, BPMN model with id modelId and selected scenarios
-   404, resources not found
-   500, internal errors

